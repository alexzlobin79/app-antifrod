package com.antifrod.utils;

import com.antifrod.model.Transfer;

import java.util.List;

public class UtilsTransfer {

    //equals elements only by senderId and recipientId!!!
    public static boolean equalsTransfers(List<Transfer> one, List<Transfer> two) {

        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        for (int i = 0; i < one.size(); i++) {

            if (!equals(one.get(i), two.get(i))) {

                return false;

            }

        }

        return true;

    }

    public static boolean equals(Transfer transfer1, Transfer transfer2) {
        if (transfer1 == transfer2) return true;

        if (transfer1 == null || transfer2 == null) return false;

        return transfer1.getSenderId() == transfer2.getSenderId() &&
                transfer1.getRecipientId() == transfer2.getRecipientId();
    }

}
