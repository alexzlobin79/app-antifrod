package com.antifrod;

import com.antifrod.logic.CycleDetector;
import com.antifrod.model.Transfer;
import com.antifrod.utils.UtilsTransfer;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CycleDetectorTest {

    @Test
    public void test_consistOneCycleTransfers() {

        List<List<Transfer>> actual = CycleDetector.get(linkedTransfersCycle(),2);

        Assert.assertTrue(UtilsTransfer.equalsTransfers(actual.get(0), linkedTransfersCycle()));

        // System.out.println(actual);

    }

    @Test
    public void test_consistTwoCycleTransfers() {

        List<List<Transfer>> actual = CycleDetector.get(linkedTransfersWithCycles(),2);

        Assert.assertTrue(UtilsTransfer.equalsTransfers(actual.get(0), linkedTransfersWithCycles()));

        System.out.println(actual);

    }

    @Test
    public void test_nonConsistOneCycleTransfers() {

        List<Transfer> copyTransfers = new ArrayList<Transfer>(linkedTransfersCycle());
        //destroy chain
        copyTransfers.remove(2);
        List<List<Transfer>> actual = CycleDetector.get(copyTransfers,2);

        Assert.assertFalse(UtilsTransfer.equalsTransfers(actual.get(0), linkedTransfersCycle()));

        // System.out.println(actual);
    }


    private List<Transfer> linkedTransfersCycle() {

        Transfer transfer1 = new Transfer(1, 1, 2);

        Transfer transfer2 = new Transfer(2, 2, 3);

        Transfer transfer3 = new Transfer(3, 4, 5);

        Transfer transfer4 = new Transfer(4, 5, 6);

        Transfer transfer5 = new Transfer(5, 5, 1);

        return Arrays.asList(transfer1, transfer2, transfer3, transfer4, transfer5);

    }

    private List<Transfer> linkedTransfersWithCycles() {

        Transfer transfer1 = new Transfer(1, 1, 2);

        Transfer transfer2 = new Transfer(2, 2, 3);

        Transfer transfer3 = new Transfer(3, 4, 5);

        Transfer transfer4 = new Transfer(4, 5, 6);

        Transfer transfer5 = new Transfer(5, 6, 100);

        Transfer transfer6 = new Transfer(100, 100, 101);

        Transfer transfer7 = new Transfer(6, 101, 6);

        Transfer transfer8= new Transfer(7, 6, 7);

        Transfer transfer9 = new Transfer(8, 7, 1);

        return Arrays.asList(transfer1, transfer2, transfer3, transfer4, transfer5,transfer6, transfer7, transfer8,transfer9);

    }

}
