package com.antifrod.service;

import com.antifrod.model.Transfer;

import java.io.IOException;
import java.util.List;

public interface ITransferFileService {

    List<Transfer> read(String path) throws IOException;

}
