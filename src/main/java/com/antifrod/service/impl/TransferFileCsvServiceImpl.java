package com.antifrod.service.impl;

import com.antifrod.error.IncorrectParsingRecord;
import com.antifrod.model.Transfer;
import com.antifrod.service.ITransferFileService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class TransferFileCsvServiceImpl implements ITransferFileService {

    private static final CSVFormat CSV_FORMAT = CSVFormat.DEFAULT;
    private static final String[] CSV_HEADERS = {"Id перевода", "Id отправителя", "Id получателя"};
    private static final char CSV_DELIMITER=';';

    @Override
    public List<Transfer> read(String path) throws IOException {

        Reader in = new FileReader(path);

        Iterable<CSVRecord> records = CSV_FORMAT
                .withHeader(CSV_HEADERS)
                .withDelimiter(CSV_DELIMITER)
                .withFirstRecordAsHeader()
                .parse(in);
        List<Transfer> transferList = new ArrayList<Transfer>();
        for (CSVRecord record : records) {

            try {
                transferList.add(map(record));
            } catch (IncorrectParsingRecord  e) {
                //skip record
                System.err.println("Incorrect parsing: " + record.toString()+" error: "+e.getCause().getMessage());

            }

        }

        return transferList;

    }

    private Long parseCsvRecord(String header, CSVRecord record) throws IncorrectParsingRecord {

        try {

            return Long.parseLong(record.get(header));

        } catch (RuntimeException e) {

            throw new IncorrectParsingRecord("Incorrect parsing record number: " + record.getRecordNumber(), e);

        }

    }

    private Transfer map(CSVRecord record) throws IncorrectParsingRecord {

        long transferId = parseCsvRecord(CSV_HEADERS[0], record);
        long senderId = parseCsvRecord(CSV_HEADERS[1], record);
        long recipientId = parseCsvRecord(CSV_HEADERS[2], record);

        return new Transfer(transferId, senderId, recipientId);

    }

}
