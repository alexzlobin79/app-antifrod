package com.antifrod.logic;


import com.antifrod.model.Transfer;

import java.util.ArrayList;
import java.util.List;

public class CycleDetector {

    public static List<List<Transfer>> get(final List<Transfer> chain, int minLenChain) {

        if (minLenChain < 2) {

            throw new IllegalArgumentException("Min length chain should be more then 1");

        }

        List<List<Transfer>> cycleChains = new ArrayList<>();

        for (int i1 = 0; i1 < chain.size()-1; i1++) {

            if (chain.size() - i1 - 1 < minLenChain) {

                break;

            }

            Transfer firstElement = chain.get(i1);

            for (int i2 = i1 + 1; i2 < chain.size(); i2++) {

                Transfer secondElement = chain.get(i2);

                if (firstElement.getSenderId() == secondElement.getRecipientId()) {

                    cycleChains.add(new ArrayList<Transfer>(chain.subList(i1, i2 + 1)));

                }

            }

        }

        return cycleChains;

    }

}