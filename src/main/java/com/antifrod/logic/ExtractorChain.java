package com.antifrod.logic;

import com.antifrod.model.Transfer;

import java.util.*;

public class ExtractorChain {

    public static final int DEFAULT_MIN_LEN_CHAIN = 3;

    private int minLenChain = DEFAULT_MIN_LEN_CHAIN;

    public ExtractorChain(int minLenChain) {

        if (minLenChain < 2) {

            throw new IllegalArgumentException("Min length chain should be more then 1");

        }

        this.minLenChain = minLenChain;
    }

    public ExtractorChain() {
    }

    public List<List<Transfer>> extract(final List<Transfer> transfers) {

        if (transfers == null || transfers.isEmpty()) {

            return Collections.emptyList();

        }

        List<List<Transfer>> chains = new ArrayList<>();

        //keep elements which are in chains
        Set<Transfer> transferSet = new HashSet<>();

        //keep elements for chain in order insert
        Set<Transfer> tempChain = new LinkedHashSet<>();

        //searching for every element chain his partner
        for (int i1 = 0; i1 < transfers.size()-1; i1++) {

            //add new chain
            if (!tempChain.isEmpty() &&tempChain.size()>=minLenChain) {
                List<Transfer> transfers1=toList(tempChain);
                chains.add(transfers1);
                transferSet.addAll(transfers1);

            }

            tempChain.clear();
            Transfer transferFirst = transfers.get(i1);

            for (int i2 = i1 + 1; i2 < transfers.size(); i2++) {

                Transfer transferNext = transfers.get(i2);

                if (!transferSet.contains(transferNext) &&!transferSet.contains(transferFirst)&& transferFirst.getRecipientId() == transferNext.getSenderId()) {


                  //  fillSets(transferSet, tempChain, transferFirst, transferNext);
                    tempChain.add(transferFirst);
                    tempChain.add(transferNext);
                    transferFirst = transferNext;

                }

            }

        }

        return chains;

    }


    private void fillSets(Set<Transfer> set1, Set<Transfer> set2, Transfer element1, Transfer element2) {

        set1.add(element1);
        set2.add(element1);

        set1.add(element2);
        set2.add(element2);

    }

    private List<Transfer> toList(Set<Transfer> set) {

        return new ArrayList<>(set);

    }

}
