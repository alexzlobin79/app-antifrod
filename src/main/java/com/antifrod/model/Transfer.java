package com.antifrod.model;

import java.util.Objects;

public class Transfer {

    private final long transferId;

    private final long senderId;

    private final long recipientId;

    public Transfer(long transferId, long senderId, long recipientId) {
        this.transferId = transferId;
        this.senderId = senderId;
        this.recipientId = recipientId;
    }

    public long getTransferId() {
        return transferId;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transfer)) return false;
        Transfer transfer = (Transfer) o;
        return transferId == transfer.transferId;
    }


    @Override
    public int hashCode() {
        return Objects.hash(transferId);
    }

    @Override
    public String toString() {
        return "<" +
                "transferId=" + transferId +
                ", senderId=" + senderId +
                ", recipientId=" + recipientId +
                '>'+"\n";
    }
}
