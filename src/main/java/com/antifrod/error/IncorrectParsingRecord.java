package com.antifrod.error;

public class IncorrectParsingRecord extends Exception {
    public IncorrectParsingRecord(String message, Throwable cause) {
        super(message, cause);
    }
}
