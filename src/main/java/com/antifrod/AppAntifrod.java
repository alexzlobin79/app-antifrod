package com.antifrod;

import com.antifrod.logic.CycleDetector;
import com.antifrod.logic.ExtractorChain;
import com.antifrod.model.Transfer;
import com.antifrod.service.ITransferFileService;
import com.antifrod.service.impl.TransferFileCsvServiceImpl;

import java.io.IOException;
import java.util.List;


public class AppAntifrod {

    private static String DEFAULT_PATH_CSV = "src/main/resources/transfers.csv";

    public static ITransferFileService transferFileService = new TransferFileCsvServiceImpl();

    public static ExtractorChain extractorChain = new ExtractorChain();

    public static void main(String[] args) {


        String pathTransfersCsv = DEFAULT_PATH_CSV;

        if (args.length > 0) {

            pathTransfersCsv = args[0];

        }

        try {
            List<Transfer> transferList = transferFileService.read(pathTransfersCsv);

            System.out.println("Loaded from: " + pathTransfersCsv + " transfers: " + transferList.size());
            List<List<Transfer>> chains = extractorChain.extract(transferList);
            System.out.println("Discovered chains =>/n");
            chains.forEach(c -> System.out.println(c));

            System.out.println("Detected cycle chains =>/n");


            for (int i = 0; i < chains.size(); i++) {

                List<List<Transfer>> chainsCycle = CycleDetector.get(chains.get(i), 3);

                if (chainsCycle.isEmpty()) {

                    System.out.println("No cycles for chain: " + i);

                    continue;

                }

                System.out.println("chain: " + i + "cycles:" + chainsCycle);

            }

        } catch (IOException e) {

            System.err.println("Incorrect path: " + pathTransfersCsv);

        }

    }

}
